//
//  TabelaEmpresas.swift
//  empresas-ios
//
//  Created by Nicolas Abrantes Bicalho on 14/11/18.
//  Copyright © 2018 Nicolas Abrantes Bicalho. All rights reserved.
//

import UIKit

class TabelaEmpresas : UITableViewController {
    
    var empresas : [DadosEmpresa] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var empresa : DadosEmpresa
        
        
        empresa = DadosEmpresa(nomeEmpresa : "Empresa1", tipoNegocio : "Negocio", nacionalidade : "Brasil", imagemEmpresa : imgE1Lista)
        empresas.append(empresa)
        
        empresa = DadosEmpresa(nomeEmpresa : "Empresa2", tipoNegocio : "Negocio2", nacionalidade : "Chile",imagemEmpresa : imgE1Lista)
        empresas.append(empresa)
        
        empresa = DadosEmpresa(nomeEmpresa : "Empresa3", tipoNegocio : "Negocio3", nacionalidade : "Peru",imagemEmpresa : imgE1Lista)
        empresas.append(empresa)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return empresas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let empresa = empresas[ indexPath.row ]
        let celulaEmpresa = "celulaEmpresa"
        let celula = tableView.dequeueReusableCell(withIdentifier: celulaEmpresa, for: indexPath)
        celula.textLabel?.text = empresa.nomeEmpresa
        
        return celula
    }
}
