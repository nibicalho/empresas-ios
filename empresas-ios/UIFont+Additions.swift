//
//  UIFont+Additions.swift
//  Teste_iOS
//
//  Generated on Zeplin. (13/11/2018).
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIFont {

  class var textStyle3: UIFont {
    return UIFont.systemFont(ofSize: 18.0, weight: .regular)
  }

  class var textStyle: UIFont {
    return UIFont(name: "Roboto-Bold", size: 16.0)!
  }

}