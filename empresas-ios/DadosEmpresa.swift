//
//  DadosEmpresa.swift
//  empresas-ios
//
//  Created by Nicolas Abrantes Bicalho on 15/11/18.
//  Copyright © 2018 Nicolas Abrantes Bicalho. All rights reserved.
//

import UIKit

class DadosEmpresa{
    
    var nomeEmpresa : String!
    var tipoNegocio : String!
    var nacionalidade : String!
    var imagemEmpresa : UIImage!
    
    init(nomeEmpresa : String, tipoNegocio : String, nacionalidade : String, imagemEmpresa : UIImage){
        self.nomeEmpresa = nomeEmpresa
        self.tipoNegocio = tipoNegocio
        self.nacionalidade = nacionalidade
        self.imagemEmpresa = imagemEmpresa
    }
    
}
